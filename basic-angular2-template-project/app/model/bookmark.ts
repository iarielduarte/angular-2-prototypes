export class Bookmark{
	constructor(
		public name:string,
		public author:string,
		public description:string,
    public url:string,
		public type:string,
		public code:string
		){}
}
