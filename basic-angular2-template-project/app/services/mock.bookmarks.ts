import { Bookmark } from "../model/bookmark";

export const BOOKMARKS: Bookmark[] = [
  new Bookmark("Curso de Angular 2 Crear web apps desde cero", "Victor Robles", "Udemy", "https://www.udemy.com/curso-de-angular-2-en-espanol-crea-webapps-desde-cero/", "Video", ""),
  new Bookmark("Accelerating Through Angular 2", "Gregg Pollack", "CodeSchool", "https://www.codeschool.com/courses/accelerating-through-angular-2", "Video", ""),
  new Bookmark("Novedades Angular 2.0", " Sergio Brito", "V2B", "https://www.video2brain.com/mx/cursos/novedades-de-angularjs-2-0", "Video", ""),
  new Bookmark("Curso Intensivo de Twitter Bootstrap", "Jesus Conde", "YouTube", "https://www.youtube.com/channel/UCbr0g_ADLsdzhcoAFUZkuug", "Video", ""),
  new Bookmark("Curso de Angular 2", "Jesus Conde", "YouTube", "https://www.youtube.com/channel/UCbr0g_ADLsdzhcoAFUZkuug", "Video", "")
];
