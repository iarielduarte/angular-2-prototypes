import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CursosListComponent } from "./components/cursos-list.component";
import { CrearCursoComponent } from "./components/crear-curso.component";
import { ContenidoComponent } from "./components/contenido.component";


const appRoutes: Routes = [
	{
		path: '',
		redirectTo: 'cursos',
		pathMatch: 'full'
	},
	{ path: "cursos", component: CursosListComponent },
	{ path: "crear", component: CrearCursoComponent },
	{ path: "crear/:nombreCurso/:plataformaCurso/:profesorCurso/:urlCurso", component: CrearCursoComponent },
	{ path: "contenido", component: ContenidoComponent }
];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
