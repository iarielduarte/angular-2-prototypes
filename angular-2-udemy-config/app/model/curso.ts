export class Curso{
	constructor(
		public id:number,
		public nombre:string,
		public profesor:string,
		public plataforma:string,
    public url:string
		){}
}
