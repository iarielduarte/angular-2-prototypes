// Importar Component desde el núcleo de Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Curso } from '../model/curso';
import { CursosService } from '../services/cursos.service';

// Decorador component, indicamos en que etiqueta se va a cargar la plantilla
@Component({
    templateUrl: 'app/views/crear-curso.html',
    providers: [CursosService]
})

// Clase del componente donde irán los datos y funcionalidades
export class CrearCursoComponent implements OnInit{
  public nuevoCurso: Curso;
  public nombreParametro = "";
  public plataforParametro = "";
  public profesorParametro = "";
  public urlParametro = "";

  // Contructor de la clase AppComponet
  constructor(private _cursosService: CursosService,
              private _router: Router,
              private _actroute: ActivatedRoute){

  }
  // Metodo de la clase CrearCursoComponent
  onSubmit(){
		this._cursosService.insertCurso(this.nuevoCurso);
		this._router.navigate(["/crear"]);
  }
  // Generador de numero al azar
  genId(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  //Meotod onInit que se ejecuta apenas se carga todo los componentes de la pagina
  ngOnInit(){
    this._actroute.params.forEach((params: Params) => {
			this.nombreParametro = params['nombreCurso'];
      this.plataforParametro = params['plataformaCurso'];
      this.profesorParametro = params['profesorCurso'];
      this.urlParametro = params['urlCurso'];
      /*Con los parametos recibidos cargo el objeto nuevoCurso para popularlo en la vista*/
      this.nuevoCurso = new Curso(this.genId(10, 1000), this.nombreParametro, this.plataforParametro, this.profesorParametro, this.urlParametro);
		});
    /*Para un solo parametro https://angular.io/docs/ts/latest/guide/router.html*/
    //this.nombreDelCurso = this._actroute.snapshot.params['nombreCurso'];
  }
}
