// Importar Component desde el núcleo de Angular
import { Component } from '@angular/core';

// Decorador component, indicamos en que etiqueta se va a cargar la plantilla
@Component({
    templateUrl: 'app/views/contenido.html'
})

// Clase del componente donde irán los datos y funcionalidades
export class ContenidoComponent {
  public titulo:string
  public estado:boolean;

  // Contructor de la clase AppComponet
  constructor(){
    this.estado = true;
    this.titulo = "Configuración Básica y Conceptos de Angular 2";

  }
  // Metodo de la clase ContenidoComponent
  readMore(st){
    this.estado = st;
  }
}
