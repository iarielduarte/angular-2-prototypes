// Importar Component desde el núcleo de Angular
import {Component} from '@angular/core';


// Decorador component, indicamos en que etiqueta se va a cargar la plantilla
@Component({
    selector: 'cursos-footer',
    templateUrl: 'app/views/cursos-footer.html'
})

// Clase del componente donde irán los datos y funcionalidades
export class CursosFooterComponent {
  public autor:string;
  public anio:number;

  // Contructor de la clase AppComponet
   constructor(){
       this.anio = 2016;
       this.autor = "Ariel Duarte";
   }
}
