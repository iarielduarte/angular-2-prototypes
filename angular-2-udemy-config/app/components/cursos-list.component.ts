// ====== ariel duarte ======

// Importar Component desde el núcleo de Angular
import {Component} from '@angular/core';
import {Curso} from '../model/curso';
import {CursosService} from '../services/cursos.service';

// Decorador component, indicamos en que etiqueta se va a cargar la plantilla
@Component({
    selector: 'cursos-list',
    templateUrl: 'app/views/cursos-list.html',
    providers: [ CursosService ]
})

// Clase del componente donde irán los datos y funcionalidades
export class CursosListComponent {
  public curso:Curso;
  public cursoSelected:Curso;
  public cursos:Array<Curso>;
  public datoService;
  // Contructor de la clase AppComponet
   constructor(private _cursosService: CursosService){
    this.cursoSelected = new Curso(1, "Curso de Angular 2 Crear web apps desde cero", "Victor Robles", "Udemy", "https://www.udemy.com/curso-de-angular-2-en-espanol-crea-webapps-desde-cero/");
    this.cursos = this._cursosService.getCursos();
    /*Usamos el Servicio*/
    this.curso = this.cursos[0];
    this.logs(this.curso);
   }

  logs(obj){
    console.log(obj);
  }

  selectCurso(curso){
    this.curso = curso;
    this.cursoSelected = curso;
  }
}
