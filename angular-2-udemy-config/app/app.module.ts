// ====== ariel duarte ======
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { AppComponent }  from './app.component';
import { routing, appRoutingProviders } from './app.routing';

import { CursosListComponent } from './components/cursos-list.component';
import { CursosFooterComponent } from './components/footer.component';
import { ContenidoComponent } from './components/contenido.component';
import { CrearCursoComponent } from './components/crear-curso.component';

@NgModule({
  imports:      [ BrowserModule, routing, FormsModule ],
  declarations: [ AppComponent, CursosListComponent, CursosFooterComponent, ContenidoComponent, CrearCursoComponent],
  providers: [ appRoutingProviders ],
  bootstrap:    [ AppComponent ]
})

export class AppModule { }
