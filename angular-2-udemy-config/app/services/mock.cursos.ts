import { Curso } from "../model/curso";

export const CURSOS: Curso[] = [
  new Curso(1, "Curso de Angular 2 Crear web apps desde cero", "Victor Robles", "Udemy", "https://www.udemy.com/curso-de-angular-2-en-espanol-crea-webapps-desde-cero/"),
  new Curso(2, "Accelerating Through Angular 2", "Gregg Pollack", "CodeSchool", "https://www.codeschool.com/courses/accelerating-through-angular-2"),
  new Curso(3, "Novedades Angular 2.0", " Sergio Brito", "V2B", "https://www.video2brain.com/mx/cursos/novedades-de-angularjs-2-0"),
  new Curso(4, "Curso Intensivo de Twitter Bootstrap", "Jesus Conde", "YouTube", "https://www.youtube.com/channel/UCbr0g_ADLsdzhcoAFUZkuug" ),
  new Curso(5, "Curso de Angular 2", "Jesus Conde", "YouTube", "https://www.youtube.com/channel/UCbr0g_ADLsdzhcoAFUZkuug")
];
