import { Injectable } from '@angular/core';
import { CURSOS } from './mock.cursos';
import { Curso } from "../model/curso";

@Injectable()
export class CursosService {
  getCursos(){
    return CURSOS;
  }

  insertCurso(curso: Curso){
		Promise.resolve(CURSOS).then((cursos: Curso[]) => cursos.push(curso));
  }
}
